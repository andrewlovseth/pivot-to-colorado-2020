<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<?php get_template_part('partials/global/google-tags'); ?>

	<?php get_template_part('partials/home/hero'); ?>

	<?php get_template_part('partials/home/companies-list'); ?>

	<?php get_template_part('partials/home/map'); ?>

	<?php get_template_part('partials/home/carousel'); ?>

	<?php get_template_part('partials/home/why-colorado'); ?>

<?php get_footer(); ?>