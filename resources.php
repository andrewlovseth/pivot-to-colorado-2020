<?php

/*

	Template Name: Resources

*/

get_header(); ?>

	<?php if(get_field('google_tags')): ?>
		<?php the_field('google_tags'); ?>
	<?php endif; ?>

	<section class="hero medium">
		<div class="wrapper">

			<div class="hero-info">
				<div class="headline text-headline">
					<h1><?php the_field('hero_headline'); ?></h1>
				</div>

				<div class="deck">
					<?php the_field('hero_deck'); ?>
				</div>
			</div>

			<div class="tabs career-resources-tabs">
				<div class="tabs-wrapper">
					<?php 
						$terms = get_field('career_resources_tabs');
						if( $terms ): ?>
					
					    <?php $count = 1; foreach( $terms as $term ): ?>
					        <a href="#" <?php if($count == 1): ?>class="active"<?php endif; ?> data-tag="<?php echo esc_html( $term->slug ); ?>"><span><?php echo esc_html( $term->name ); ?></span></a>
					    <?php $count++; endforeach; ?>

					<?php endif; ?>
				</div>

			</div>

		</div>
	</section>

	<section id="results" class="career-resources">
		<div class="wrapper">

			<section id="posts">

				<section id="response">

				</section>

			</section>

		</div>
	</section>


<?php get_footer(); ?>