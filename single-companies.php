<?php get_header(); ?>

	<?php if(get_field('google_tags')): ?>
		<?php the_field('google_tags'); ?>
	<?php endif; ?>

	<section class="hero medium" class="cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<h1 class="invisible"><?php the_title(); ?></h1>

		</div>
	</section>

	<section id="hero-info">
		<div class="wrapper">
			<a href="<?php echo site_url('/companies/'); ?>" class="back-to-all"><span>Back to all</span></a>

			<div class="info">

				<div class="header">
					<div class="logo">
						<img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<div class="tagline">
						<p><?php the_field('tagline'); ?></p>
					</div>

					<div class="careers-link">
						<a href="#careers" class="arrow">Featured Careers</a>
					</div>
				</div>

				<div class="description">
					<?php the_field('description'); ?>
				</div>

			</div>

		</div>
	</section>


	<section id="content">
		<div class="wrapper">

			<?php the_field('main_content'); ?>

		</div>
	</section>


	<?php if(have_rows('careers_carousel') && get_field('carousel_headline')): ?>
		<section id="carousel-header">
			<div class="wrapper">
				
				<h2><?php the_field('carousel_headline'); ?></h2>
			</div>
		</section>
	<?php endif; ?>

	<?php get_template_part('partials/carousel'); ?>


	<section id="careers">
		<div class="wrapper">

			<?php $posts = get_field('featured_careers'); if( $posts ): ?>

				<h2>Featured careers at <?php the_title(); ?></h2>
	
			    <?php foreach( $posts as $post): setup_postdata($post); ?>

			        <div class="career">
			        	<h4><a href="<?php the_field('link'); ?>" target="_blank"><?php the_title(); ?></a></h4>
			        </div>

			    <?php endforeach; ?>

			<?php wp_reset_postdata(); endif; ?>

			<div class="meta">
				<div class="view-all">
					<a href="<?php the_field('jobs_url'); ?>" target="_blank" class="btn">View all careers at <?php the_title(); ?></a>
				</div>

				<div class="social">
					<h4>Other ways to connect to this company:</h4>

					<?php if(have_rows('connect_links')): while(have_rows('connect_links')) : the_row(); ?>
					 
					    <?php if( get_row_layout() == 'website' ): ?>							
							<div class="link website">
					    		<a href="<?php the_sub_field('link'); ?>" target="_blank">
					    			<img src="<?php bloginfo('template_directory') ?>/images/website.svg" />
					    		</a>
							</div>							
					    <?php endif; ?>

					    <?php if( get_row_layout() == 'linkedin' ): ?>							
							<div class="link linkedin">
					    		<a href="<?php the_sub_field('link'); ?>" target="_blank">
					    			<img src="<?php bloginfo('template_directory') ?>/images/linkedin.svg" />
					    		</a>
							</div>							
					    <?php endif; ?>

					    <?php if( get_row_layout() == 'built_in_colorado' ): ?>							
							<div class="link built-in-colorado">
					    		<a href="<?php the_sub_field('link'); ?>" target="_blank">
					    			<img src="<?php bloginfo('template_directory') ?>/images/built-in-colorado.svg" />
					    		</a>
							</div>							
					    <?php endif; ?>

					    <?php if( get_row_layout() == 'glassdoor' ): ?>							
							<div class="link glassdoor">
					    		<a href="<?php the_sub_field('link'); ?>" target="_blank">
					    			<img src="<?php bloginfo('template_directory') ?>/images/glassdoor.svg" />
					    		</a>
							</div>							
					    <?php endif; ?>


					    <?php if( get_row_layout() == 'the_muse' ): ?>							
							<div class="link the-muse">
					    		<a href="<?php the_sub_field('link'); ?>" target="_blank">
					    			<img src="<?php bloginfo('template_directory') ?>/images/the-muse.svg" />
					    		</a>
							</div>							
					    <?php endif; ?> 
					<?php endwhile; endif; ?>
				</div>
			</div>

		</div>
	</section>

	<?php get_template_part('partials/share'); ?>

<?php get_footer(); ?>