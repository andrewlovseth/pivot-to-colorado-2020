<?php

/*

    ----------------------------------------------------------------------
    					XX Theme Support
    ----------------------------------------------------------------------

*/

show_admin_bar( false );

function register_my_menu() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_my_menu' );

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function custom_tiled_gallery_width() {
    return '1200';
}
add_filter( 'tiled_gallery_content_width', 'custom_tiled_gallery_width' );

add_theme_support( 'title-tag' );


add_filter( 'the_seo_framework_use_archive_title_prefix', '__return_false' );


function my_robots_adjustments( $meta = array() ) {

    if ( is_singular('careers') ) {
        $meta['noindex'] = 'noindex';
        $meta['nofollow'] = 'nofollow';
        $meta['noarchive'] = 'noarchive';
    }

    return $meta;
}
add_filter( 'the_seo_framework_robots_meta_array', 'my_robots_adjustments', 10, 1 );


add_filter( 'the_seo_framework_sitemap_exclude_cpt', function( $excluded = array() ) {
    $excluded[] = 'careers';
    return $excluded;
} );



add_filter( 'the_seo_framework_description_output', 'seo_careers_description', 10, 2 );

function seo_careers_description( $description = '', $id = 0 ) {
    if ( is_post_type_archive( 'careers' ) ) {
        $description = 'Browse featured Colorado tech jobs. Find your prefect career fit in Engineering and Technology, Product and Design, Data and Analytics or Sales and Marketing.';
    }
    return $description;
}



// Enqueue custom styles and scripts
function enqueue_styles_and_scripts() {
    // Register and noConflict jQuery 3.2.1
    wp_register_script( 'jquery.3.2.1', 'https://code.jquery.com/jquery-3.2.1.min.js' );
    wp_add_inline_script( 'jquery.3.2.1', 'var jQuery = $.noConflict(true);' );

    $uri = get_stylesheet_directory_uri();
    $dir = get_stylesheet_directory();

    $script_last_updated_at = filemtime($dir . '/js/site.js' );
    $style_last_updated_at = filemtime($dir . '/style.css' );

    // Add style.css
    wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css' , '', $style_last_updated_at);

    // Add plugins.js & site.js (with jQuery dependency)
    wp_enqueue_script( 'custom-plugins', get_stylesheet_directory_uri() . '/js/plugins.js', array( 'jquery.3.2.1' ), $script_last_updated_at, true );
    wp_enqueue_script( 'custom-site', get_stylesheet_directory_uri() . '/js/site.js', array( 'jquery.3.2.1' ), $script_last_updated_at, true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_styles_and_scripts' );





/*

    ----------------------------------------------------------------------
    					XX Code Cleanup
    ----------------------------------------------------------------------

*/

remove_action( 'wp_head', 'wp_resource_hints', 2 );
remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action( 'wp_head', 'wp_generator');
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}
 
// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );
 
// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );






/*

    ----------------------------------------------------------------------
    					XX Custom Functions
    ----------------------------------------------------------------------

*/

/* Filter <p>'s on <img> and <iframe>' */
function filter_ptags_on_images($content) {
	$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	return preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '\1', $content);
}
add_filter('the_content', 'filter_ptags_on_images');
add_filter('acf/format_value/type=wysiwyg', 'filter_ptags_on_images', 10, 3);


/* List of Tag Slugs */
function entry_tags() {
	$posttags = get_the_tags();
	if ($posttags) {
	  foreach($posttags as $tag) {
	    echo $tag->slug; 
	  }
	}
}

function replace_admin_menu_icons_css() {
    ?>
    <style>

		.dashicons-admin-post:before,
		.dashicons-format-standard:before {
		    content: "\f331";
		}

    </style>
    <?php
}

function remove_menus(){
  remove_menu_page( 'edit.php' );                   //Posts
  remove_menu_page( 'edit-comments.php' );          //Comments
}
add_action( 'admin_menu', 'remove_menus' );


function custom_menu_order($menu_ord) {
    if (!$menu_ord) return true;
     
    return array(
        'index.php', // Dashboard
        'separator1', // First separator
        'edit.php?post_type=page', // Pages
        'edit.php?post_type=companies', // Companies
        'edit.php?post_type=careers', // Careers
        'edit.php?post_type=events', // Events
        'edit.php?post_type=highlights', // Highlights
        'edit.php?post_type=community_members', // Community Members
        'edit.php?post_type=career_resources', // Career Resources
        'upload.php', // Media
        'separator2', // Second separator
        'themes.php', // Appearance
        'plugins.php', // Plugins
        'users.php', // Users
        'tools.php', // Tools
        'options-general.php', // Settings
        'separator-last', // Last separator
    );
}
add_filter('custom_menu_order', 'custom_menu_order'); // Activate custom_menu_order
add_filter('menu_order', 'custom_menu_order');





// CAREERS

//Enqueue Ajax Scripts
function enqueue_career_filter_ajax_scripts() {
    wp_register_script('career-filter', get_bloginfo('template_url') . '/js/career-filter.js', '', true );
    wp_localize_script('career-filter', 'ajax_listing_params', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

    if (is_post_type_archive( 'careers')) {
        wp_enqueue_script('career-filter', get_bloginfo('template_url') . '/js/career-filter.js','','',true);
    }
}
add_action('wp_enqueue_scripts', 'enqueue_career_filter_ajax_scripts');

//Add Ajax Actions
add_action('wp_ajax_career_filter', 'ajax_career_filter');
add_action('wp_ajax_nopriv_career_filter', 'ajax_career_filter');


function ajax_career_filter() {

    $query_data = $_GET;
    $cat_term = $query_data['categories'];
    $company = $query_data['companies'];

    $careers_args = array(
        'post_type' => 'careers',
        'posts_per_page' => 150,
        'orderby' => 'rand',
        'category_name' => $cat_term,
        'meta_key'     => 'company',
        'meta_value'   => $company,
        'type'      => 'NUMERIC',
        'meta_compare' => '='
    );

    $careers_loop = new WP_Query($careers_args);

    if( $careers_loop->have_posts() ): while( $careers_loop->have_posts() ): $careers_loop->the_post();
        get_template_part('partials/career');
    endwhile;   
    else:
        get_template_part('partials/career-none');
    endif;

    wp_reset_postdata();

    die();
}








// COMMUNITY

//Enqueue Ajax Scripts
function enqueue_community_filter_ajax_scripts() {
    wp_register_script('community-filter', get_bloginfo('template_url') . '/js/community-filter.js', '', true );
    wp_localize_script('community-filter', 'ajax_listing_params', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

    if(is_page_template('community.php')) {
        wp_enqueue_script('community-filter', get_bloginfo('template_url') . '/js/community-filter.js','','',true);
    }
}
add_action('wp_enqueue_scripts', 'enqueue_community_filter_ajax_scripts');

//Add Ajax Actions
add_action('wp_ajax_community_filter', 'ajax_community_filter');
add_action('wp_ajax_nopriv_community_filter', 'ajax_community_filter');

function ajax_community_filter() {

    $query_data = $_GET;
    $tags = $query_data['tags'];

    $community_args = array(
        'post_type' => 'community_members',
        'posts_per_page' => 150,
        'order' => 'ASC',
        'orderby' => 'menu_order',
        'tag' => $tags,
    );

    $community_loop = new WP_Query($community_args);

    if( $community_loop->have_posts() ): while( $community_loop->have_posts() ): $community_loop->the_post();
        get_template_part('partials/community/member');
    endwhile;   
    else:
        get_template_part('partials/community/member-none');
    endif;

    wp_reset_postdata();

    die();
}








// RESOURCES

//Enqueue Ajax Scripts
function enqueue_resource_filter_ajax_scripts() {
    wp_register_script('resource-filter', get_bloginfo('template_url') . '/js/resource-filter.js', '', true );
    wp_localize_script('resource-filter', 'ajax_listing_params', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

    if(is_page_template('resources.php')) {
        wp_enqueue_script('resource-filter', get_bloginfo('template_url') . '/js/resource-filter.js','','',true);
    }
  
}
add_action('wp_enqueue_scripts', 'enqueue_resource_filter_ajax_scripts');

//Add Ajax Actions
add_action('wp_ajax_resource_filter', 'ajax_resource_filter');
add_action('wp_ajax_nopriv_resource_filter', 'ajax_resource_filter');

function ajax_resource_filter() {

    $query_data = $_GET;
    $tags = $query_data['tags'];

    $resources_args = array(
        'post_type' => 'career_resources',
        'posts_per_page' => 150,
        'order' => 'ASC',
        'orderby' => 'menu_order',
        'tag' => $tags,
    );

    $resources_loop = new WP_Query($resources_args);

    if( $resources_loop->have_posts() ): while( $resources_loop->have_posts() ): $resources_loop->the_post();
        get_template_part('partials/career-resources/resource');
    endwhile;   
    else:
        get_template_part('partials/career-resources/resource-none');
    endif;

    wp_reset_postdata();

    die();
}





/*

    ----------------------------------------------------------------------
    					XX Advanced Custom Fields
    ----------------------------------------------------------------------

*/

function my_relationship_query( $args, $field, $post_id ) {
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    return $args;
}


if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}

// filter for every field
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);

function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

	</style>

	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');

function carrers_relationship_result( $title, $post, $field, $post_id ) {
    
    // load a custom field from this $object and show it in the $result
    $company = get_field('company', $post->ID);
    
    
    // append to title
    $title .= ' (' . get_the_title($company) .  ')';

    
    // return
    return $title;
    
}
add_filter('acf/fields/relationship/result/key=field_5a8cf57956ee9', 'carrers_relationship_result', 10, 4);


function my_acf_init() {
    
    acf_update_setting('google_api_key', 'AIzaSyBKB1PAq-nV6E5XrI004P_5rnCAr0dtGDA');
}

add_action('acf/init', 'my_acf_init');


add_filter( 'gform_field_content', function ( $field_content, $field ) {
    if ( $field->type == 'email' ) {
        return str_replace( 'type=', "autocomplete='email' type=", $field_content );
    }

    if ( $field->type == 'name' ) {
        return str_replace( 'type=', "autocomplete='name' type=", $field_content );
    }
 
    return $field_content;
}, 10, 2 );

