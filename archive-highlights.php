<?php get_header(); ?>

	<section class="hero small">
		<div class="wrapper">

			<div class="hero-info">
				<div class="svg-header">

					<div class="mobile">
						<h1>Highlights</h1>
					</div>
				</div>

			</div>

		</div>
	</section>

	<section id="list">
		<div class="wrapper">

			<?php echo do_shortcode('[ajax_load_more id="highlights" container_type="div" post_type="highlights" posts_per_page="6" scroll="false" button_label="Load More"]'); ?>		

		</div>
	</section>
	
<?php get_footer(); ?>