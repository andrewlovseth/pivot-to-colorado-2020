<?php

/*

	Template Name: Stay Updated

*/

get_header(); ?>

	<?php if(get_field('google_tags')): ?>
		<?php the_field('google_tags'); ?>
	<?php endif; ?>

	<section class="hero">
		<div class="wrapper">

			<div class="hero-info">
				<div class="svg-header">
					<div class="desktop">
						<img src="<?php $image = get_field('svg_header'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<div class="mobile">
						<h1><?php the_field('hero_headline'); ?></h1>
					</div>
				</div>		

				<div class="deck">
					<?php the_field('hero_deck'); ?>
				</div>				
			</div>
			
			<div class="stay-updated-form">
				<?php $stay_updated_form = get_field('stay_updated_form', 'options'); echo do_shortcode($stay_updated_form); ?>
			</div>

			<div class="copyright">
				<?php the_field('copyright', 'options'); ?>
			</div>

		</div>
	</section>

<?php get_footer(); ?>