<?php

/*

	Template Name: Community

*/

get_header(); ?>

	<?php if(get_field('google_tags')): ?>
		<?php the_field('google_tags'); ?>
	<?php endif; ?>

	<section class="hero medium">
		<div class="wrapper">

			<div class="hero-info">
				<div class="headline text-headline">
					<h1><?php the_field('hero_headline'); ?></h1>
				</div>

				<div class="deck">
					<?php the_field('hero_deck'); ?>
				</div>
			</div>

			<div class="tabs community-tabs">
				<div class="tabs-wrapper">
					<a href="#" data-tag="" class="all active">All</a>

					<?php 
						$terms = get_field('community_tabs');
						if( $terms ): ?>
					
					    <?php foreach( $terms as $term ): ?>
					        <a href="#" data-tag="<?php echo esc_html( $term->slug ); ?>"><?php echo esc_html( $term->name ); ?></a>
					    <?php endforeach; ?>

					<?php endif; ?>
				</div>
			</div>

		</div>
	</section>

	<section id="results" class="community">
		<div class="wrapper">

			<section id="posts">

				<section id="response">

				</section>

			</section>

		</div>
	</section>

	<section class="institutions">
		<div class="wrapper">

			<div class="institutions-container">
				<?php if(have_rows('institutions')): while(have_rows('institutions')) : the_row(); ?>
	 
				    <?php if( get_row_layout() == 'group' ): ?>
						
						<div class="group">
							<div class="group-header">
								<h3><?php the_sub_field('header'); ?></h3>
							</div>

							<div class="group-grid">
								<?php if(have_rows('members')): while(have_rows('members')): the_row(); ?>
								 
								    <div class="item">
								    	<a href="<?php the_sub_field('link'); ?>" rel="external">
								    		<img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								    	</a>							        
								    </div>

								<?php endwhile; endif; ?>							
							</div>			    		
						</div>
						
				    <?php endif; ?>
				 
				<?php endwhile; endif; ?>
			</div>

		</div>
	</section>

<?php get_footer(); ?>