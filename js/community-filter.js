jQuery(function($) {

	//Load posts on page load
	community_get_posts();

	//Find Selected Categories
	function getSelectedTags() {
		var tag = $('.community-tabs a.active').attr('data-tag');	
		return tag;
	}

	//On tab/filter change
	$('.community-tabs a').on('click', function(){
		community_get_posts();
	});
	
	//Main ajax function
	function community_get_posts() {
		var ajax_url = ajax_listing_params.ajax_url;

		$.ajax({
			type: 'GET',
			url: ajax_url,
			data: {
				action: 'community_filter',
				tags: getSelectedTags
			},
			beforeSend: function () {
				$('body.page-template-community #response').removeClass('transition');
				$('body.page-template-community #response').addClass('fade-out');
			},
			success: function(data) {
				$('body.page-template-community #response').html(data);

				setTimeout(function(){
					$('body.page-template-community #response').removeClass('fade-out');
					$('body.page-template-community #response').addClass('transition');
				}, 200);				

				$('.member a').on('click', function() {
					var communityLink = $(this).attr('href');
					var trackerName = ga.getAll()[0].get('name');

					ga(trackerName + '.send', 'event', {
						eventCategory: 'Outbound Link',
						eventAction: 'click',
						eventLabel: communityLink
					});
				});

			},
			error: function() {
				$("body.page-template-community #response").html('<p>There has been an error</p>');
			}
		});				
	}
	
});