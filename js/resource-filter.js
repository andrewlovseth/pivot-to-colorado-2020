jQuery(function($) {

	//Load posts on page load
	resource_get_posts();

	//Find Selected Categories
	function getSelectedTags() {
		var tag = $('.career-resources-tabs a.active').attr('data-tag');	
		return tag;
	}

	//On tab/filter change
	$('.career-resources-tabs a').on('click', function(){
		resource_get_posts();
	});
	
	//Main ajax function
	function resource_get_posts() {
		var ajax_url = ajax_listing_params.ajax_url;

		$.ajax({
			type: 'GET',
			url: ajax_url,
			data: {
				action: 'resource_filter',
				tags: getSelectedTags
			},
			beforeSend: function () {
				$('body.page-template-resources #response').removeClass('transition');
				$('body.page-template-resources #response').addClass('fade-out');
			},
			success: function(data) {
				$('body.page-template-resources #response').html(data);

				setTimeout(function(){
					$('body.page-template-resources #response').removeClass('fade-out');
					$('body.page-template-resources #response').addClass('transition');
				}, 200);

				$('.resource a').on('click', function() {
					var resourceLink = $(this).attr('href');
					var trackerName = ga.getAll()[0].get('name');

					ga(trackerName + '.send', 'event', {
						eventCategory: 'Outbound Link',
						eventAction: 'click',
						eventLabel: resourceLink
					});
				});

			},
			error: function() {
				$("body.page-template-resources #response").html('<p>There has been an error</p>');
			}
		});				
	}
	
});