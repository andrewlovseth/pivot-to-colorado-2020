(function ($, window, document, undefined) {

	$(document).ready(function($) {

		// 01 GLOBAL

		// rel="external"
		$('a[rel="external"]').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});
		

		// Menu Toggle
		$('#toggle').click(function(){
			$('header, nav').toggleClass('open');
			return false;
		});


		// FitVids
		$('.embed').fitVids();


		// Notch Smooth Scroll
		$('.notch a').on('click', function() {
			$.smoothScroll({
				scrollTarget: 'footer'
			});
			return false;
		});


		// Career Carousel
		$('#carousel').slick({
			dots: true,
			arrows: true,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			slidesToScroll: 1,
			mobileFirst: true,
			adaptiveHeight: true
		});


		// Career Carousel
		$('#home-career-carousel').slick({
			dots: false,
			arrows: true,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			slidesToScroll: 1,
			mobileFirst: true,
			adaptiveHeight: true

		});

		// Add current index to page
		$('#home-career-carousel').on('afterChange', function(event, slick, currentSlide, nextSlide){

			var slideIndex = currentSlide + 1;
			console.log(slideIndex);

			$('.pagination .current').html(slideIndex);
		});


		// 
		$('.locations-dropdown .selected').on('click', function(){
			$('.locations-dropdown .options').slideToggle();
		});

		$('.locations-dropdown .options .location').on('click', function(){
			var state = $(this).data('state');
			var currentCity = $(this).data('city');

			$('.locations-dropdown .selected').html(currentCity);

			$('.locations .location-table').removeClass('active');
			$('.locations').find('.location-table[data-state="' + state + '"]').addClass('active');

			$('.locations-dropdown .options').slideToggle();
		});



		// Open Video
		$('#carousel .video .launch').on('click', function(){
			var video = $(this).closest('.video');
			var overlay = $(video).find('.overlay');

			$(overlay).addClass('open');

			return false;
		});


		// Close Video
		function closeVideo() {
			$('.overlay').removeClass('open');
		}

		$('.video .close').on('click', function(){
			closeVideo();

			return false;
		});


		// Tabs
		$('.tabs a').on('click', function(){
			$('.tabs a').removeClass('active');
			$(this).addClass('active')

			return false;
		});


		// Footer Form Floodlight Tags

	    jQuery(document).bind('gform_confirmation_loaded', function(event, formId){
	        if(formId == 1) {
	        	callStaticFL('8503708', 'conve0', 'stayu0+standard')
	        }
	    });


		// 02 HOMEPAGE

		$('body.home .view-source a').on('click', function(){
			$('.view-source .copy').slideToggle();

			$(this).text($(this).text() == 'Close' ? 'View our sources' : 'Close');

			return false;
		});


		// Responsive Image Map
	    $('map').imageMapResize();

	    // Interactive Map
	    $('body.home .interactive-map area').on('click', function(){
	    	var state = $(this).data('state');

	    	$('.pop-ups, #' + state).addClass('show');

			return false;
	    });

		$(window).click(function() {
			$('body.home .pop-up, body.home .pop-ups').removeClass('show');
		});

		$('body.home .pop-up table').click(function(event){
		    event.stopPropagation();
		});





		// 03 FEATURED CAREERS

		// Filters on Archive Careers
		$('.companies-dropdown .list a').on('click', function(){
			$('.companies-dropdown .list a').removeClass('active');
			$(this).addClass('active');
			var activeFilterText = $('.companies-dropdown .list a.active').text();
			$('.companies-dropdown .selected span').text(activeFilterText);
			$('.companies-dropdown .list, .companies-dropdown .selected span').toggleClass('open');
			return false;

		});

		// Active Filter
		var activeFilterText = $('.companies-dropdown .list a.active').text();
		$('.companies-dropdown .selected span').text(activeFilterText);

		$('.companies-dropdown .selected span').on('click', function(){
			$('.companies-dropdown .list').toggleClass('open');
			$(this).toggleClass('open');

			return false;
		});




		// 04 COMPANY PROFILE

		// Careers Link on Single Company
		$('.careers-link a').smoothScroll();

		$('body.single-companies .career a').on('click', function() {
			var careerLink = $(this).attr('href');
			var trackerName = ga.getAll()[0].get('name');

			ga(trackerName + '.send', 'event', {
				eventCategory: 'Outbound Link',
				eventAction: 'click',
				eventLabel: careerLink
			});
		});



		// 06 ABOUT

		// Facts Carousel on About
		$('#facts-carousel').slick({
			dots: true,
			arrows: true,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			slidesToScroll: 1,
			mobileFirst: true,
		});




	    // COMPANIES Interactive Map

	    // POI Click
	    $('.companies-map .poi').on('click', function(){
	    	var poi = $(this).data('item');

	    	// Add/Remove Active Class
	    	$('.companies-map .poi').removeClass('active');
	    	$(this).toggleClass('active');

	    	// Activate/Deactive Legend Item
	    	var legenedItem = $('.companies-map .legend').find('.item[data-item="' + poi + '"]');
	    	$('.companies-map .legend .item').removeClass('active');
	    	$(legenedItem).toggleClass('active');

			return false;
	    });


	    // Legend Click
	    $('.companies-map .legend .item').on('click', function(){
	    	var legend = $(this).data('item');

	    	// Add/Remove Active Class
	    	$('.companies-map .legend .item').removeClass('active');
	    	$(this).toggleClass('active');

	    	// Activate/Deactive Legend Item
	    	var poi = $('.companies-map .poi[data-item="' + legend + '"]');
	    	$('.companies-map .poi').removeClass('active');
	    	$(poi).toggleClass('active');

			return false;
	    });


	    // POI Hover
	    $('.companies-map .poi').hover(
			function() {

				// Mouseover
				var poi = $(this).data('item');
		    	var legenedItem = $('.companies-map .legend').find('.item[data-item="' + poi + '"]');
		    	$(legenedItem).toggleClass('active-highlight');

			}, function() {

				// Mouseout
				var poi = $(this).data('item');
		    	var legenedItem = $('.companies-map .legend').find('.item[data-item="' + poi + '"]');
		    	$(legenedItem).toggleClass('active-highlight');

			}
		);


	    // Legend Hover
	    $('.companies-map .legend .item').hover(
			function() {

				// Mouseover
				var legend = $(this).data('item');
		    	var poi = $('.companies-map .poi[data-item="' + legend + '"]');
		    	$(poi).toggleClass('active-highlight');

			}, function() {

				// Mouseout
				var legend = $(this).data('item');
		    	var poi = $('.companies-map .poi[data-item="' + legend + '"]');
		    	$(poi).toggleClass('active-highlight');

			}
		);


	});


	// Esc to close video modal
	$(document).on('keyup',function(evt) {
	    if (evt.keyCode == 27) {
			$('.overlay, .fact-overlay').removeClass('open');
			$('body.home .pop-up, body.home .pop-ups').removeClass('show');
	    }
	});

})(jQuery, window, document);