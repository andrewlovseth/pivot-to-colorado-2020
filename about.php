<?php

/*

	Template Name: About

*/

get_header(); ?>

	<?php if(get_field('google_tags')): ?>
		<?php the_field('google_tags'); ?>
	<?php endif; ?>

	<section class="hero">
		<div class="wrapper">

			<div class="hero-info">
				<div class="svg-header">
					<div class="desktop">
						<img src="<?php $image = get_field('svg_header'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<div class="mobile">
						<h1><?php the_field('hero_headline'); ?></h1>
					</div>
				</div>			

				<div class="deck">
					<?php the_field('hero_deck'); ?>
				</div>				
			</div>

			<div class="notch">
				<p>Colorado Tech Scene by the Numbers</p>
			</div>
		</div>
	</section>


	<section id="facts">
		<div class="wrapper">

			<section id="facts-carousel">

				<?php if(have_rows('facts_carousel')): while(have_rows('facts_carousel')): the_row(); ?>

					<div class="slide">
						<div class="slide-wrapper">
							<h2><?php the_sub_field('headline'); ?></h2>

							<?php if(get_sub_field('sub_headline')): ?>
								<h3><?php the_sub_field('sub_headline'); ?></h3>
							<?php endif; ?>

							<?php if(get_sub_field('source')): ?>
								<h4><?php the_sub_field('source'); ?></h4>
							<?php endif; ?>
						</div>
					</div>

				<?php endwhile; endif; ?>

			</section>

		</div>
	</section>
	
	<section id="content">

		<section id="partners">
			<div class="wrapper">

				<h4>Partners</h4>

				<?php $posts = get_field('partners_list', 'options'); if( $posts ): ?>

					<div class="companies-list-wrapper partners">

					    <?php foreach( $posts as $post): setup_postdata($post); ?>

					    	<?php get_template_part('partials/partners'); ?>

					    <?php endforeach; ?>

					</div>
				
				<?php wp_reset_postdata(); endif; ?>

			</div>
		</section>


		<section id="companies">
			<div class="wrapper">

				<h4>Participating Companies</h4>

				<?php $posts = get_field('companies_list', 'options'); if( $posts ): ?>

					<div class="companies-list-wrapper">

					    <?php foreach( $posts as $post): setup_postdata($post); ?>

					    	<?php get_template_part('partials/company'); ?>

					    <?php endforeach; ?>

					</div>
				
				<?php wp_reset_postdata(); endif; ?>

				<div class="cta">
					<a href="<?php echo site_url('/companies/'); ?>" class="btn">View all featured careers at these companies</a>
				</div>

			</div>
		</section>

	</section>


<?php get_footer(); ?>