<div class="company <?php echo $post->post_name; ?>">
    <div class="bevel"></div>
    <div class="border"></div>
    <div class="color" style="border-color: <?php the_field('key_color'); ?>  transparent transparent transparent;"></div>

    <div class="logo">
        <a href="<?php the_permalink(); ?>">
            <img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        </a>
    </div>

    <div class="tagline">
        <p><?php the_field('tagline'); ?></p>
    </div>

    <div class="links">
        <a href="<?php the_permalink(); ?>">Details</a> | <a href="<?php the_permalink(); ?>#careers">Careers</a>
    </div>
</div>