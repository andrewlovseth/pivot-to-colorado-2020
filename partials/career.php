<div class="career">
	<div class="link">
		<h4>
			<a href="<?php $link = get_field('link'); echo $link; ?>" target="_blank" onClick='callFloodlight_new("8503708", "conve0", "featu0");'>
				<span><?php the_title(); ?></span>
			</a>
		</h4>
	</div>

	<div class="meta">
		<?php $company = get_field('company'); if( $company ): ?>

		<div class="logo">
			<a href="<?php echo get_permalink($company); ?>">
				<img src="<?php $image = get_field('logo', $company); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		</div>
		<div class="links">
			<a href="<?php echo get_permalink($company); ?>">Company Details</a>
			<a href="<?php echo get_field('jobs_url', $company); ?>" target="_blank">All Careers</a>
		</div>

		<?php endif; ?>
	</div>				
</div>