<div class="svg-header">
	<div class="desktop">
		<img src="<?php $image = get_field('svg_header'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>

	<div class="mobile">
		<img src="<?php $image = get_field('mobile_svg_header'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>
</div>
