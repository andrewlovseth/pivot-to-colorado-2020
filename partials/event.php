<div class="event">
	<div class="image">
		<a href="<?php the_permalink(); ?>">
			<img src="<?php $image = get_field('featured_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>
	</div>

	<div class="info">
		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<h4><?php the_field('dates'); ?></h4>

		<div class="description">
			<?php the_field('short_description'); ?>
		</div>

		<a href="<?php the_permalink(); ?>" class="read-more">Read more</a>
	</div>
</div>