<div class="partner">
    <div class="logo">
        <a href="http://<?php the_field('url'); ?>/" target="_blank">
            <img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        </a>
    </div>

    <div class="info">
        <h3><?php the_title(); ?></h3>
        <a class="url" href="http://<?php the_field('url'); ?>/" target="_blank"><?php the_field('url'); ?></a>
    </div>

    <div class="description">
        <?php the_field('short_description'); ?>
    </div>
</div>