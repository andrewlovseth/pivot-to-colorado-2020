<div class="member">
	<div class="border"></div>
	<div class="bevel"></div>

	<div class="photo">
		<div class="content">
			<img src="<?php $image = get_field('photo'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>		
	</div>
	
	<div class="info">
		<div class="name">
			<h3><?php the_title(); ?></h3>
		</div>

		<div class="url">
			<a class="arrow" href="<?php the_field('url'); ?>" target="_window"><?php the_field('vanity_url'); ?></a>
		</div>
	</div>
</div>