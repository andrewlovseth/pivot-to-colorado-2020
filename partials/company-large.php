<div class="company company-large">
	<div class="bevel"></div>
	<div class="border"></div>
	<div class="color" style="border-color: <?php the_field('key_color'); ?>  transparent transparent transparent;"></div>

	<div class="logo">
    	<a href="<?php the_permalink(); ?>">
    		<img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
    	</a>
    </div>

    <div class="info">
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

        <div class="tagline">
        	<p><?php the_field('tagline'); ?></p>
        </div>

        <div class="description">
            <p><?php echo wp_trim_words( get_field('short_description'), 12 ); ?> <a href="<?php the_permalink(); ?>" class="read-more">Read More</a></p>
        </div>
    </div>

    <div class="links">
        <div class="link">
        	<a href="<?php the_permalink(); ?>" class="arrow">View Company Details</a>
        </div>
    </div>

    <div class="bevel-bottom"></div>
    <div class="border-bottom"></div>
</div>