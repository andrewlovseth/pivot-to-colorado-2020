<section class="list">
	<div class="wrapper">
	
		<?php $posts = get_field('companies_list', 'options'); if( $posts ): ?>

			<div class="companies-list-wrapper">

			    <?php foreach( $posts as $post): setup_postdata($post); ?>

			    	<?php get_template_part('partials/company-large'); ?>

			    <?php endforeach; ?>

			</div>
		
		<?php wp_reset_postdata(); endif; ?>

	</div>
</section>