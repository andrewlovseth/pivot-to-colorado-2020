<section class="map companies-map">
	<div class="wrapper">

		<div class="section-header">
			<div class="headline">
				<h2><?php the_field('company_map_headline', 'options'); ?></h2>
			</div>

			<div class="deck">
				<?php the_field('company_map_deck', 'options'); ?>
			</div>
		</div>

		<div class="mobile-map">
			<img src="<?php $image = get_field('company_mobile_map', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			
		</div>

		<div class="interactive-map">
			<div class="map-image">
				<?php if(have_rows('company_map_legend', 'options')): while(have_rows('company_map_legend', 'options')): the_row(); ?>
				 
			    	<?php $coords = get_sub_field('coordinates'); ?>
			    	<div class="poi" data-item="item-<?php the_sub_field('number'); ?>" style="left: <?php echo $coords['x']; ?>%; top: <?php echo $coords['y']; ?>%;">
			    		<span><?php the_sub_field('number'); ?></span>
			    	</div>				        

				<?php endwhile; endif; ?>	


				<?php if(have_rows('company_office_locations', 'options')): while(have_rows('company_office_locations', 'options')): the_row(); ?>
				 
			    	<?php
			    		$coords = get_sub_field('coordinates');
			    		$logo = get_sub_field('logo');
			    		$post_object = get_sub_field('company'); if( $post_object ): $post = $post_object; setup_postdata( $post );
			    	?>

			    	<a href="<?php the_permalink(); ?>" class="poi-company" data-item="<?php echo sanitize_title_with_dashes(get_the_title()); ?>" style="left: <?php echo $coords['x']; ?>%; top: <?php echo $coords['y']; ?>%;">

				    		<span><img src="<?php echo $logo['sizes']['medium']; ?>" alt="<?php echo $logo['alt']; ?>" /></span>

			    		<?php wp_reset_postdata(); endif; ?>
			    		
			    	</a>				        

				<?php endwhile; endif; ?>	

				<img src="<?php $image = get_field('company_map', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="legend">
				<?php if(have_rows('company_map_legend', 'options')): while(have_rows('company_map_legend', 'options')): the_row(); ?>
				 
				    <div class="item" data-item="item-<?php the_sub_field('number'); ?>">
				    	<div class="number">
				    		<span><?php the_sub_field('number'); ?></span>
				    	</div>
				    	<div class="info">
				    		<div class="name"><?php the_sub_field('name'); ?></div>
				    	</div>
			    		<div class="description">
			    			<?php the_sub_field('description'); ?>
			    		</div>
				        
				    </div>

				<?php endwhile; endif; ?>					
			</div>
		</div>

	</div>
</section>