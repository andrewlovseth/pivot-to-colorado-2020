<section class="hero small">
	<div class="wrapper">

		<div class="hero-info">

			<div class="svg-header">
				<div class="desktop">
					<img src="<?php $image = get_field('companies_svg_header', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="mobile">
					<h1><?php the_field('company_profiles_headline', 'options'); ?></h1>
				</div>
			</div>

			<div class="deck">
				<?php the_field('company_profiles_deck', 'options'); ?>
			</div>
			
		</div>

	</div>
</section>