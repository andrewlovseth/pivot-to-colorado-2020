<section class="logos">
	<div class="wrapper">

		<?php $posts = get_field('companies_list', 'options'); if( $posts ): ?>

			<div class="logo-grid">

				<?php foreach( $posts as $post): setup_postdata($post); ?>

					<div class="logo">
				    	<a href="<?php the_permalink(); ?>">
				    		<img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    	</a>
				    </div>

				<?php endforeach; ?>

			</div>
		
		<?php wp_reset_postdata(); endif; ?>

	</div>
</section>