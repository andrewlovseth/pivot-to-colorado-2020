<section id="why-colorado">
	<div class="wrapper">

		<div class="section-header">
			<h2><?php the_field('why_colorado_headline'); ?></h2>
			<p><?php the_field('why_colorado_deck'); ?></p>
		</div>

		<div id="why-carousel">
			
			<?php if(have_rows('why_colorado_carousel')): while(have_rows('why_colorado_carousel')): the_row(); ?>
			 	<div class="slide">
				    <div class="stat">
				    	<h4><?php the_sub_field('stat'); ?></h4>
				    </div>

				    <div class="info">
				    	<h2><?php the_sub_field('headline'); ?></h2>
				    	<?php if(get_sub_field('sub_headline')): ?>
					    	<h3><?php the_sub_field('sub_headline'); ?></h3>
					    <?php endif; ?>
				    </div>				 		
			 	</div>

			<?php endwhile; endif; ?>

		</div>

	</div>
</section>