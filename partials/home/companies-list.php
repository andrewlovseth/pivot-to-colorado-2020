<section class="companies-list">
	<div class="wrapper">

		<div class="section-header">
			<div class="headline">
				<h2><?php the_field('companies_headline'); ?></h2>
			</div>

			<div class="deck">
				<?php the_field('companies_deck'); ?>
			</div>
		</div>

		<?php $posts = get_field('companies_list', 'options'); if( $posts ): ?>

			<div class="companies-list-wrapper">

			    <?php foreach( $posts as $post): setup_postdata($post); ?>

			    	<?php get_template_part('partials/company-minimal'); ?>

			    <?php endforeach; ?>

			</div>
		
		<?php wp_reset_postdata(); endif; ?>

		<div class="cta">

			<a href="<?php echo site_url('/companies/'); ?>" class="btn">Company Profiles ></a>

		</div>

	</div>
</section>