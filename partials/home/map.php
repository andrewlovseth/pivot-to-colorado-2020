<section class="map">
	<div class="wrapper">

		<div class="section-header">
			<div class="headline">
				<h2><?php the_field('map_headline'); ?></h2>
			</div>

			<div class="deck">
				<?php the_field('map_deck'); ?>
			</div>
		</div>

		<div class="mobile-table" data-location-active="">
			<div class="select">
				<span>Select City to Compare</span>

				<div class="locations-dropdown">
					<div class="location selected">
						<?php
							$locations = get_field('map_info');
							$first_city = $locations[0]['opposing_location'];
							echo $first_city;
						?>
					</div>
					<div class="options">
						<?php $count = 1; if(have_rows('map_info')): while(have_rows('map_info')): the_row(); ?>
							<div class="location" data-city="<?php the_sub_field('opposing_location'); ?>" data-state="<?php the_sub_field('state'); ?>"><?php the_sub_field('opposing_location'); ?></div>
						<?php $count++; endwhile; endif; ?>		
					</div>
			
				</div>
			</div>

			<div class="locations">
				<?php $count = 1; if(have_rows('map_info')): while(have_rows('map_info')): the_row(); ?>

					<div class="location-table <?php if($count == 1): ?> active<?php endif; ?>" data-state="<?php the_sub_field('state'); ?>">
						<div class="location-header">
							<div class="denver-col data">Denver</div>
							<div class="opposing-col data"><?php the_sub_field('opposing_location'); ?></div>
						</div>
		    			<?php if(have_rows('table_rows')): while(have_rows('table_rows')): the_row(); ?>
		 				    <div class="row">
						    	<div class="header-col"><?php the_sub_field('header_col'); ?></div>
						    	<div class="denver-col data">
						    		<?php $denver_col = get_sub_field('denver_col'); ?>
						    		<span class="<?php echo $denver_col['color']; ?>"><?php echo $denver_col['text']; ?></span>
						    	</div>
						    	<div class="opposing-col data">
						    		<?php $opposing_col = get_sub_field('opposing_col'); ?>
						    		<span class="<?php echo $opposing_col['color']; ?>"><?php echo $opposing_col['text']; ?></span>
						    	</div>		        
						    </div>
						<?php endwhile; endif; ?>
					</div>				 

				<?php $count++; endwhile; endif; ?>
			</div>
		</div>

		<div class="interactive-map">
			<img src="<?php $image = get_field('map_image'); echo $image['url']; ?>" usemap="#image-map">

			<map name="image-map">
			    <area target="" alt="WA" data-state="WA" href="" coords="211,38,183,126,325,169,345,71" shape="poly">
			    <area target="" alt="N-CA" data-state="N-CA" href="" coords="167,214,244,238,224,310,243,347,169,362,136,265" shape="poly">
			    <area target="" alt="S-CA" data-state="S-CA" href="" coords="165,365,179,413,234,486,297,492,320,457,253,346" shape="poly">
			    <area target="" alt="CA" data-state="CA" href="" coords="151,209,248,241,225,302,323,457,277,506,228,483,176,418,141,264" shape="poly">
			    <area target="" alt="UT" data-state="UT" href="" coords="351,262,326,380,421,397,436,299,400,296,402,272" shape="poly">
			    <area target="" alt="TX" data-state="TX" href="" coords="544,429,535,538,455,535,517,625,553,608,617,698,662,709,663,656,733,611,744,551,735,495,602,477,608,431" shape="poly">
			    <area target="" alt="IL" data-state="IL" href="" coords="781,274,849,271,854,376,828,418,798,416,768,337,771,299" shape="poly">
			    <area target="" alt="NY" data-state="NY" href="" coords="987,271,986,232,1003,212,1027,193,1057,169,1084,167,1100,221,1104,267" shape="poly">
			    <area target="" alt="MA" data-state="MA" href="" coords="1101,256,1100,228,1125,187,1211,173,1213,243,1156,261" shape="poly">
			    <area target="" alt="DC" data-state="DC" href="" coords="1021,298,1022,362,1139,362,1137,296" shape="poly">
			    <area target="" alt="NC" data-state="NC" href="" coords="972,401,1090,381,1106,408,1057,472,995,452,959,449,927,450" shape="poly">
			    <area target="" alt="GA" data-state="GA" href="" coords="903,457,920,551,935,563,987,559,1021,524,950,453" shape="poly">
			    <area target="" alt="FL" data-state="FL" href="" coords="870,563,886,580,936,594,973,641,1019,700,1069,689,1052,622,1022,560,1002,560,914,562" shape="poly">
			</map>

			<div class="pop-ups">				
				<?php if(have_rows('map_info')): while(have_rows('map_info')): the_row(); ?>
				 
				    <div class="pop-up" id="<?php the_sub_field('state'); ?>">
				    	<table>
				    		<thead>
				    			<tr>
				    				<th class="header-col">Denver <span class="versus">vs.</span> <?php the_sub_field('opposing_location'); ?></th>
				    				<th class="denver-col data">Denver</th>
				    				<th class="opposing-col data"><?php the_sub_field('opposing_location'); ?></th>
				    			</tr>
				    		</thead>
				    		<tbody>
				    			<?php if(have_rows('table_rows')): while(have_rows('table_rows')): the_row(); ?>
				 				    <tr>
								    	<td class="header-col"><?php the_sub_field('header_col'); ?></td>
								    	<td class="denver-col data">
								    		<?php $denver_col = get_sub_field('denver_col'); ?>
								    		<span class="<?php echo $denver_col['color']; ?>"><?php echo $denver_col['text']; ?></span>
								    	</td>
								    	<td class="opposing-col data">
								    		<?php $opposing_col = get_sub_field('opposing_col'); ?>
								    		<span class="<?php echo $opposing_col['color']; ?>"><?php echo $opposing_col['text']; ?></span>
								    	</td>		        
								    </tr>
								<?php endwhile; endif; ?>
				    		</tbody>
				    	</table>
				    </div>

				<?php endwhile; endif; ?>
			</div>
		</div>

		<div class="view-source">
			<div class="copy">
				<?php the_field('source_copy'); ?>
			</div>
			<div class="src-btn">
				<a href="#">View Our Sources</a>
			</div>				
		</div>

	</div>
</section>