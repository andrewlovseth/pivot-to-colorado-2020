<section class="hero">
	<div class="content">
		<img class="hero-cover-image" src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

		<div class="hero-content">
			<div class="wrapper">

				<div class="info">
					<h1 class="invisible"><?php echo get_bloginfo('name'); ?></h1>
					
					<div class="svg-header">
						<div class="desktop">
							<img src="<?php $image = get_field('svg_header'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>

						<div class="mobile">
							<img src="<?php $image = get_field('mobile_svg_header'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>
					</div>

					<div class="about-statement">
						<?php the_field('about_statement'); ?>
					</div>
				</div>

				<div class="cta">	
					<a href="<?php echo site_url('/careers'); ?>" class="btn">See Colorado Tech Jobs ></a>
				</div>

			</div>			
		</div>
		
	</div>
</section>