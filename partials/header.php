	<header>
		<div class="wrapper">

			<div class="logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<a href="#" id="toggle">
				<div class="patty"></div>
			</a>

			<nav>
				<div class="nav-wrapper">
					<?php if(have_rows('nav', 'options')): while(have_rows('nav', 'options')): the_row(); ?>
					 
					    <a href="<?php the_sub_field('link'); ?>" class="nav-<?php echo sanitize_title_with_dashes(get_sub_field('label')); ?>">
					        <?php the_sub_field('label'); ?>
					    </a>

					<?php endwhile; endif; ?>

					<a href="<?php echo site_url('/stay-updated/'); ?>" class="btn">Stay Updated</a>
				</div>
			</nav>

		</div>
	</header>