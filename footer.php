	<footer>
		<div class="wrapper">

			<div class="social">

				<h5>Connect with us</h5>

				<div class="links">
					<?php if(have_rows('social_links', 'options')): while(have_rows('social_links', 'options')): the_row(); ?>
						<a href="<?php the_sub_field('link'); ?>" target="_blank">
							<img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</a>
					<?php endwhile; endif; ?>
				</div>

			</div>

			<div class="polygon-container">
				<div class="info">
					<div class="headline">
						<h2><?php the_field('footer_headline', 'options'); ?></h2>
					</div>

					<div class="deck">
						<?php the_field('footer_deck', 'options'); ?>
					</div>

					<div class="cta">
						<a href="<?php echo site_url('/stay-updated'); ?>" class="btn">Stay Updated ></a>
					</div>
				</div>					
			</div>

			<div class="copyright">

				<div class="links">

					<?php if(have_rows('footer_nav', 'options')): while(have_rows('footer_nav', 'options')): the_row(); ?>
					 
					    <a href="<?php the_sub_field('link'); ?>" class="nav-<?php echo sanitize_title_with_dashes(get_sub_field('label')); ?>">
					        <?php the_sub_field('label'); ?>
					    </a>

					<?php endwhile; endif; ?>					
				</div>


				<div class="message">
					<?php the_field('copyright', 'options'); ?>
				</div>

			</div>

		</div>
	</footer>

	<?php get_template_part('partials/fact-overlay'); ?>
	
	<?php wp_footer(); ?>

	<?php get_template_part('partials/google-analytics'); ?>

</body>
</html>