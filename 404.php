<?php get_header(); ?>

	<section class="hero">
		<div class="wrapper">

			<div class="hero-info">
				<h1><?php the_field('404_headline', 'options'); ?></h1>
				<div class="deck">
					<?php the_field('404_copy', 'options'); ?>
				</div>				
			</div>

		</div>
	</section>
	   	
<?php get_footer(); ?>