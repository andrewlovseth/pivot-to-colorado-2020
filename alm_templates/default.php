<div class="news">
	<div class="image">
		<div class="content">
    		<a href="<?php the_field('url'); ?>" target="_blank">
				<img src="<?php $image = get_field('publication_logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
    		</a>					        			
		</div>
	</div>

	<div class="info">
		<h3><a href="<?php the_field('url'); ?>" target="_blank"><?php the_title(); ?></a></h3>
		<h4><?php the_time('F j, Y'); ?></h4>

		<a href="<?php the_field('url'); ?>" target="_blank" class="read-more">Read the Article</a>
	</div>
</div>