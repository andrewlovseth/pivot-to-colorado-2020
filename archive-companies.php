<?php get_header(); ?>

	<?php if(get_field('companies_google_tags', 'options')): ?>
		<?php the_field('companies_google_tags', 'options'); ?>
	<?php endif; ?>

	<?php get_template_part("partials/companies/hero"); ?>

	<?php get_template_part("partials/companies/map"); ?>

	<?php get_template_part("partials/companies/list"); ?>
	
<?php get_footer(); ?>
