<!DOCTYPE html>
<html>
<head>
	<meta name="google-site-verification" content="h273VYqiu3HiC_hKdapMWRss8JIXcGiOLDGtPJIQsns" />
	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<?php if(get_field('global_google_tags', 'options')): ?>
		<?php the_field('global_google_tags', 'options'); ?>
	<?php endif; ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<?php if(get_field('stay_updated_google_tags', 'options')): ?>
		<?php the_field('stay_updated_google_tags', 'options'); ?>
	<?php endif; ?>

	<?php if(is_page_template( 'homepage.php' )): ?>

		<?php get_template_part('partials/home-header'); ?>

	<?php else: ?>

		<?php get_template_part('partials/site-header'); ?>

	<?php endif; ?>